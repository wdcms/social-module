<?php

/*
 * This file is part of the xbhub\socialite.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Modules\Social\Api;

interface ProviderInterface
{
    /**
     * Redirect the user to the authentication page for the provider.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     *
     * @param \Modules\Social\Api\AccessTokenInterface $token
     *
     * @return \Modules\Social\Api\User
     */
    public function user(AccessTokenInterface $token = null);
}
