<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_user_account', function (Blueprint $table) {
            $table->id();

            $table->string('driver');
            $table->string('openid');
            $table->string('anonymous_openid')->nullable();
            $table->string('token')->nullable();
            $table->string('nickname')->comment('昵称');
            $table->string('avatar')->comment('头像');
            $table->string('gender')->nullable();
            $table->string('country')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();

            $table->tenant();
            $table->createby();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_user_account');
    }
}
