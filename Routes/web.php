<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Modules\Social\Http\Controllers\OAuthController;
use Modules\Social\Http\Controllers\SocialController;

// oauth
Route::get('/oauth/{driver}', [OAuthController::class, 'redirectToProvider'])->name('oauth');
Route::get('/callback/oauth/{driver}', [OAuthController::class, 'handleProviderCallback']);

Route::middleware('admin:social')->group(function () {

    Route::resource('user-social', SocialController::class);

});
