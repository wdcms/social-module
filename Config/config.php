<?php

return [
    'name' => 'Social',
    'options' => [
        // ['name' => 'CORE_GLOBAL_TITLE', 'label' => '系统名称', 'default' => 'Lumina'],

        //社交登陆 ==========================
        ['group' => '飞书', 'name' => 'SOCIAL_OAUTH_FEISHU_ENABLE', 'label' => '是否启用', 'default' => '0'],
        ['group' => '飞书', 'name' => 'SOCIAL_OAUTH_FEISHU_CLIENT_ID', 'label' => 'client_id', 'default' => ''],
        ['group' => '飞书', 'name' => 'SOCIAL_OAUTH_FEISHU_CLIENT_SECRET', 'label' => 'client_secret', 'default' => ''],
        ['group' => '飞书', 'name' => 'SOCIAL_OAUTH_FEISHU_REDIRECT', 'label' => 'redirect', 'default' => url('callback/oauth/feishu'), 'disabled' => true],

        //微信 ==========================
        ['group' => '企业微信', 'name' => 'SOCIAL_OAUTH_WEWORK_ENABLE', 'label' => '是否启用', 'default' => '0'],
        ['group' => '企业微信', 'name' => 'SOCIAL_OAUTH_WEWORK_CLIENT_ID', 'label' => 'client_id', 'default' => ''],
        ['group' => '企业微信', 'name' => 'SOCIAL_OAUTH_WEWORK_CLIENT_SECRET', 'label' => 'client_secret', 'default' => ''],
        ['group' => '企业微信', 'name' => 'SOCIAL_OAUTH_WEWORK_REDIRECT', 'label' => 'redirect', 'default' => url('callback/oauth/wechat'), 'disabled' => true],

    ],
];
